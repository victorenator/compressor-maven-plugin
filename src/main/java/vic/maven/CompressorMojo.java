package vic.maven;

import com.nixxcode.jvmbrotli.common.BrotliLoader;
import com.nixxcode.jvmbrotli.enc.BrotliOutputStream;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.FileSystems;
import static java.nio.file.Files.copy;
import static java.nio.file.Files.createDirectories;
import static java.nio.file.Files.delete;
import static java.nio.file.Files.getLastModifiedTime;
import static java.nio.file.Files.isRegularFile;
import static java.nio.file.Files.newOutputStream;
import static java.nio.file.Files.size;
import static java.nio.file.Files.walk;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;
import java.util.List;
import static java.util.stream.Collectors.toList;
import java.util.zip.GZIPOutputStream;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

@Mojo(name = "compress", defaultPhase = LifecyclePhase.PROCESS_RESOURCES)
public class CompressorMojo extends AbstractMojo {

    @Parameter(required = true)
    Path sourceDirectory;

    @Parameter(defaultValue = "${project.build.outputDirectory}")
    Path outputDirectory;

    @Parameter
    List<String> includes = List.of("**/*.css", "**/*.mjs", "**/*.js", "**/*.svg");
    
    @Parameter
    List<String> excludes = List.of();

    @Parameter(defaultValue = "0")
    long minSize = 0;

    @Parameter(defaultValue = "1")
    double minFactor = 1;
    
    @Parameter(defaultValue = "true")
    boolean brotli = true;
    
    @Parameter(defaultValue = "true")
    boolean gzip = true;

    private List<PathMatcher> includeMatchers;
    private List<PathMatcher> excludeMatchers;

    @Override
    public void execute() throws MojoExecutionException {
        try {
            if (gzip || brotli && BrotliLoader.isBrotliAvailable()) {
                final var fs = FileSystems.getDefault();
                includeMatchers = includes.stream().map("glob:"::concat).map(fs::getPathMatcher).collect(toList());
                excludeMatchers = excludes.stream().map("glob:"::concat).map(fs::getPathMatcher).collect(toList());
                getLog().info("Compressing resources in " + sourceDirectory + " …");
                try (var stream = walk(sourceDirectory, Integer.MAX_VALUE)) {
                    stream.filter(this::isMachedFile).forEach(this::compress);
                }
            }

        } catch (final IOException | UncheckedIOException ex) {
            throw new MojoExecutionException("IO exception when compressing files", ex);
        }
    }

    private boolean isMachedFile(final Path file) {
        try {
            return isRegularFile(file, LinkOption.NOFOLLOW_LINKS) &&
                    size(file) > minSize &&
                    includeMatchers.stream().anyMatch(pm -> pm.matches(file)) &&
                    excludeMatchers.stream().noneMatch(pm -> pm.matches(file));
            
        } catch (final IOException ex) {
            throw new UncheckedIOException(ex);
        }
    }
    
    private void compress(final Path sourceFile) {
        if (brotli && BrotliLoader.isBrotliAvailable()) {
            compressBrotli(sourceFile);
        }
        if (gzip) {
            compressGzip(sourceFile);
        }
    }

    private void compressBrotli(final Path sourceFile) {
        try {
            final Path rel = sourceDirectory.relativize(sourceFile);
            final Path targetFile = outputDirectory.resolve(rel.toString().concat(".br"));

            if (isRegularFile(targetFile) && getLastModifiedTime(targetFile).compareTo(getLastModifiedTime(sourceFile)) > 0) {
                getLog().debug("Skipped file " + sourceFile + " is up to date");
                return;
            }

            createDirectories(targetFile.getParent());
            try (var out = new BrotliOutputStream(newOutputStream(targetFile))) {
                copy(sourceFile, out);
            }
            final long sourceSize = size(sourceFile);
            final long targetSize = size(targetFile);
            if (sourceSize > minFactor * targetSize) {
                getLog().info("Brotli " + sourceFile + " ("  + sourceSize + " -> " + targetSize + ")");

            } else {
                getLog().debug("Brotli " + sourceFile + " ("  + sourceSize + " -> " + targetSize + ") removing, compressed version is bigger");
                delete(targetFile);
            }
        } catch (final IOException ex) {
            throw new UncheckedIOException(ex);
        }
    }

    private void compressGzip(final Path sourceFile) {
        try {
            final var rel = sourceDirectory.relativize(sourceFile);
            final var targetFile = outputDirectory.resolve(rel.toString().concat(".gz"));

            if (isRegularFile(targetFile) && getLastModifiedTime(targetFile).compareTo(getLastModifiedTime(sourceFile)) > 0) {
                getLog().debug("Skipped file " + sourceFile + " is up to date");
                return;
            }

            createDirectories(targetFile.getParent());
            try (var out = new GZIPOutputStream(newOutputStream(targetFile))) {
                copy(sourceFile, out);
            }
            final long sourceSize = size(sourceFile);
            final long targetSize = size(targetFile);
            if (sourceSize > minFactor * targetSize) {
                getLog().info("Gzip " + sourceFile + " ("  + sourceSize + " -> " + targetSize + ")");
                
            } else {
                getLog().debug("Gzip " + sourceFile + " ("  + sourceSize + " -> " + targetSize + ") removing, compressed version is bigger");
                delete(targetFile);
            }
        } catch (final IOException ex) {
            throw new UncheckedIOException(ex);
        }
    }

    public void setSourceDirectory(final String sourceDirectory) {
        this.sourceDirectory = Paths.get(sourceDirectory);
    }

    public void setOutputDirectory(final String outputDirectory) {
        this.outputDirectory = Paths.get(outputDirectory);
    }
}
