package vic.maven;

import java.io.IOException;
import static java.nio.file.Files.isRegularFile;
import java.nio.file.Paths;
import static junit.framework.TestCase.assertTrue;
import org.apache.maven.plugin.MojoExecutionException;
import org.junit.Before;
import org.junit.Test;

public class CompressorMojoTest {

    final CompressorMojo compressMojo = new CompressorMojo();

    @Before
    public void init() throws IOException {
        compressMojo.outputDirectory = Paths.get("target/resources");
        compressMojo.sourceDirectory = Paths.get("src/test/resources/resources");
    }

    @Test
    public void execute() throws MojoExecutionException {
        compressMojo.execute();
        assertTrue(isRegularFile(Paths.get("target/resources/default.css.br")));
        assertTrue(isRegularFile(Paths.get("target/resources/default.css.gz")));
    }
}
