# Compressor Maven Plugin

Compresses static resources using gzip

## Options

* sourceDirectory: path
* outputDirectory: path = ${project.build.directory}/${project.build.finalName}
* includes: list = [`**/*.css`, `**/*.mjs`, `**/*.js`, `**/*.svg`]
* excludes: list = []
* minSize: int = 0
* minFactor: float = 1
* brotli: bool = true
* gzip: bool = true

## Usage
```xml
<pom>
...

  <build>
    <plugins>
      <plugin>
        <groupId>vic</groupId>
        <artifactId>compressor-maven-plugin</artifactId>
        <version>1</version>
        <configuration>
          <sourceDirectory>${project.build.outputDirectory}</sourceDirectory>
        </configuration>
        <executions>
          <execution>
            <goals>
              <goal>compress</goal>
            </goals>
          </execution>
        </executions>
      </plugin>
    </plugins>
  </build>

</pom>
```
